from os import getenv

from flask import Flask, url_for, flash
from flask import request
from flask import render_template
from flask import redirect
from flask_login import login_required
from flask_login import LoginManager, login_user

from flask_migrate import Migrate

from models import db
from models.user import User

from views.mink import mink_app
from views.products import products_app




app = Flask(__name__)
app.register_blueprint(products_app)
app.register_blueprint(mink_app)

config_class_name = getenv("CONFIG_CLASS", "DevelopmentConfig")
config_object = f"config.{config_class_name}"
app.config.from_object(config_object)

db.init_app(app)
migrate = Migrate(app=app, db=db)
manager = LoginManager(app)

@manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(username=username).first()
        if user and user.check_password(password):
            login_user(user)
            return redirect(url_for('index'))
        flash('Invalid username or password')

    if request.method == 'GET':
        return render_template('login.html')

@app.get("/", endpoint="index")
@login_required
def hello_root():
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
