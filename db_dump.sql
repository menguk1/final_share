--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Debian 15.3-1.pgdg120+1)
-- Dumped by pg_dump version 15.3 (Debian 15.3-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: username
--

CREATE TABLE public.alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE public.alembic_version OWNER TO username;

--
-- Name: mink; Type: TABLE; Schema: public; Owner: username
--

CREATE TABLE public.mink (
    id integer NOT NULL,
    "MSISDN" character varying,
    subscription_id character varying,
    created timestamp without time zone,
    status integer
);


ALTER TABLE public.mink OWNER TO username;

--
-- Name: mink_id_seq; Type: SEQUENCE; Schema: public; Owner: username
--

CREATE SEQUENCE public.mink_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mink_id_seq OWNER TO username;

--
-- Name: mink_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: username
--

ALTER SEQUENCE public.mink_id_seq OWNED BY public.mink.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: username
--

CREATE TABLE public.product (
    id integer NOT NULL,
    name character varying NOT NULL,
    serv_id integer NOT NULL,
    price integer NOT NULL
);


ALTER TABLE public.product OWNER TO username;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: username
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO username;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: username
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: subscription; Type: TABLE; Schema: public; Owner: username
--

CREATE TABLE public.subscription (
    id integer NOT NULL,
    serv_id integer NOT NULL,
    name character varying NOT NULL,
    price integer NOT NULL
);


ALTER TABLE public.subscription OWNER TO username;

--
-- Name: subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: username
--

CREATE SEQUENCE public.subscription_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subscription_id_seq OWNER TO username;

--
-- Name: subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: username
--

ALTER SEQUENCE public.subscription_id_seq OWNED BY public.subscription.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: username
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(64),
    password_hash character varying
);


ALTER TABLE public."user" OWNER TO username;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: username
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO username;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: username
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: mink id; Type: DEFAULT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.mink ALTER COLUMN id SET DEFAULT nextval('public.mink_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: subscription id; Type: DEFAULT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.subscription ALTER COLUMN id SET DEFAULT nextval('public.subscription_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: username
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: username
--

COPY public.alembic_version (version_num) FROM stdin;
f78570c2dbe1
\.


--
-- Data for Name: mink; Type: TABLE DATA; Schema: public; Owner: username
--

COPY public.mink (id, "MSISDN", subscription_id, created, status) FROM stdin;
5	79044040809	1000	2023-11-30 17:48:26.723644	1
6	79089080803	1001	2023-11-30 18:15:05.01381	1
10	79085001020	1002	2023-12-02 14:52:59.730407	1
11	79041005050	1003	2023-12-02 14:54:50.661232	1
12	79014008899	1004	2023-12-02 14:57:13.488242	1
13	79046567799	1005	2023-12-02 15:01:28.220854	1
14	79048889462	1005	2023-12-02 15:02:30.788271	1
15	79054940808	1006	2023-12-02 15:07:39.009385	1
19	79885446698	1005	2023-12-02 16:33:11.728711	1
20	79885446699	1005	2023-12-02 16:33:35.080218	1
21	79083411798	1006	2023-12-02 16:34:15.659793	1
22	79515146236	1006	2023-12-02 16:36:24.089393	1
25	79915146598	1005	2023-12-02 16:44:05.041008	1
2	79515554422	1000	2023-11-29 19:14:22.379	1
26	79515441052	1002	2023-12-03 16:18:43.696788	1
\.


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: username
--

COPY public.product (id, name, serv_id, price) FROM stdin;
1	Free	1000	0
7	Basic	1002	150
8	Free	1000	0
9	MoreFilms	1003	300
10	Child	1006	400
11	Classic	1007	400
\.


--
-- Data for Name: subscription; Type: TABLE DATA; Schema: public; Owner: username
--

COPY public.subscription (id, serv_id, name, price) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: username
--

COPY public."user" (id, username, password_hash) FROM stdin;
1	operator.anna	scrypt:32768:8:1$wUjqi8ql9TCw1lbW$de1b9533c9e8e19b5086c26f56c3d15cbe056dc0eab3f463975399ec4c29917eb41cfb2ee9161f2b1c4329d934f5043652a3d5804340cee6ab9d2c199b2d5e11
\.


--
-- Name: mink_id_seq; Type: SEQUENCE SET; Schema: public; Owner: username
--

SELECT pg_catalog.setval('public.mink_id_seq', 26, true);


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: username
--

SELECT pg_catalog.setval('public.product_id_seq', 11, true);


--
-- Name: subscription_id_seq; Type: SEQUENCE SET; Schema: public; Owner: username
--

SELECT pg_catalog.setval('public.subscription_id_seq', 1, false);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: username
--

SELECT pg_catalog.setval('public.user_id_seq', 1, true);


--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: mink mink_pkey; Type: CONSTRAINT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.mink
    ADD CONSTRAINT mink_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: subscription subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: username
--

ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: username
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user user_username_key; Type: CONSTRAINT; Schema: public; Owner: username
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- PostgreSQL database dump complete
--

