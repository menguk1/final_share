from typing import TYPE_CHECKING
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import Column, Integer, String

from .database import db

if TYPE_CHECKING:
    from flask_sqlalchemy.query import Query


class User(UserMixin, db.Model):
    id = Column(Integer, primary_key=True)
    username = Column(String(64), unique=True)
    password_hash = Column(String)

    def set_password(self, password):
        generate_password = generate_password_hash(password)
        self.password_hash = generate_password

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    if TYPE_CHECKING:
        query: Query
