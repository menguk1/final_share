from typing import TYPE_CHECKING
import datetime
from sqlalchemy import Column, Integer, String, DateTime

from .database import db

if TYPE_CHECKING:
    from flask_sqlalchemy.query import Query


class Mink(db.Model):
    id = Column(Integer, primary_key=True, autoincrement=True)
    MSISDN = Column(String, nullable=True)
    subscription_id = Column(String, nullable=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    status = Column(Integer, nullable=True)

    if TYPE_CHECKING:
        query: Query
