import requests
from flask import request
from flask import Blueprint
from flask import render_template

from models import db, Mink
from .forms.mink import MinkForm


mink_app = Blueprint(
    "mink_app",
    __name__,
    url_prefix="/mink",
)

@mink_app.get("/", endpoint="list")
def get_mink_list():
    minks: list[Mink] = Mink.query.order_by(Mink.id).all()
    return render_template("mink/list.html", minks=minks)


@mink_app.route("/search/", methods=["GET", "POST"], endpoint="search")
def get_mink_by_msisdn():
    form = MinkForm()
    if request.method == "GET":
        return render_template("mink/search_by_msisdn.html", form=form)

    if request.method == "POST":
        number = form.data['MSISDN']
        print(number)
        minks: list[Mink] = Mink.query.filter_by(MSISDN=number)
        return render_template("mink/list.html", minks=minks, form=form)

@mink_app.route("/add/", methods=["GET", "POST"], endpoint="add")
def add_new_row():
    form = MinkForm()
    if request.method == "GET":
        return render_template("mink/add.html", form=form)

    if not form.validate_on_submit():
        return render_template("mink/add.html", form=form), 400

    if request.method == "POST":
        payload = {'MSISDN': form.data['MSISDN'], 'subscription': form.data["subscription_id"] }
        r = requests.post(' https://reqres.in/api/users?page=2',
                          data=payload, timeout=10)# отправка запроса партнёру
        print(r.text)

        row = Mink(MSISDN=form.data['MSISDN'],
                   subscription_id=form.data["subscription_id"],
                   status='1')
        print(row)
        db.session.add(row)
        db.session.commit()
        minks: list[Mink] = Mink.query.filter_by(MSISDN=form.data['MSISDN'])
        return render_template("mink/list.html", minks=minks, form=form, response_text=r.text)




@mink_app.route("/delete/", methods=["GET", "POST"], endpoint="delete")
def delete_row():
    form = MinkForm()
    if request.method == "GET":
        return render_template("mink/delete.html", form=form)

    if request.method == "POST":
        payload = {'MSISDN': form.data['MSISDN'], 'subscription': form.data["subscription_id"]}
        r = requests.patch(' https://reqres.in/api/users/2',
                           data=payload,
                           timeout=10) # отправка запроса партнёру
        print(r.text)
        number = form.data['MSISDN']
        subscription_from_form = form.data["subscription_id"]
        Mink.query.filter_by(MSISDN=number,
                             subscription_id=subscription_from_form).delete()
        db.session.commit()
        minks: list[Mink] = Mink.query.filter_by(MSISDN=number,
                                                 subscription_id=subscription_from_form)
        return render_template("mink/list.html", minks=minks, form=form, response_text=r.text)

