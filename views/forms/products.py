from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length


class ProductForm(FlaskForm):
    name = StringField(
        #label="Product name:",
        label="Subscription name:",
        validators=[
            DataRequired(),
            Length(min=3),
        ],
    )
    price = StringField(
        label="Subscription price:",
        validators=[
            DataRequired(),
            Length(min=1),
        ],
    )

    serv_id = StringField(
        label="Subscription serv_id:",
        validators=[
            DataRequired(),
            Length(min=3),
        ],
    )
