from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length


class MinkForm(FlaskForm):
    subscription_id = StringField(
        label="Subscription ID:",
        validators=[
            DataRequired(),
            Length(min=3),
        ],
    )
    MSISDN = StringField(
        label="MSISDN:",
        validators=[
            DataRequired(),
            Length(min=11),
        ],
    )
